
resource "local_file" "AnsibleInventory" {
  content = templatefile("templates/hosts.tpl", {
    manager-ip = aws_eip.manager.*.public_ip
    worker-ip = aws_eip.worker.*.public_ip
    })
  filename = "inventory/hosts.yaml"
}

resource "null_resource" "ansible_load" {

  provisioner "local-exec" {
    command = "sleep 40"
  }

  # Provision First Manager
  # Hard Set manager.0. whilst we have an issue obtaining LoadBalancer IP via Terraform
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory/hosts.yaml rke2_server.yaml --extra-vars managerip='${aws_instance.manager.0.private_ip}'"

  }

  # Provision HA Managers
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory/hosts.yaml rke2_ha_server.yaml --extra-vars managerip='${aws_instance.manager.0.private_ip}'"

  }

  # Provision workers
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory/hosts.yaml rke2_worker.yaml --extra-vars managerip='${aws_instance.manager.0.private_ip}'"

  }
  depends_on = [aws_eip_association.eip_manager]
}

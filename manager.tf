resource "aws_instance" "manager" {

  count                  = var.quantity-manager
  ami                    = var.ami
  instance_type          = "z4.xlarge"
  subnet_id              = var.subnet
  vpc_security_group_ids = [aws_security_group.allow_all.id]
  key_name               = var.keyname
  
  user_data = <<-EOF
    #! /bin/bash
    sudo sed -i "/search symphony.local/c\search symphony.local ""${var.domainname}" /etc/resolv.conf
  EOF

  root_block_device {
    volume_size          = "50"
  }

  tags = {
    Name                 = "${var.managername}${count.index}"
  }

}

resource "aws_network_interface" "manager" {

  subnet_id              = var.subnet
  security_groups        = [aws_security_group.allow_all.id]

}

resource "aws_eip" "manager" {
  count                  = var.quantity-manager
  vpc                    = true
}

resource "aws_eip_association" "eip_manager" {

  instance_id            = element(aws_instance.manager.*.id,count.index)
  network_interface_id   = aws_network_interface.manager.id
  allocation_id          = element(aws_eip.manager.*.id,count.index)
  count                  = var.quantity-manager

}

resource "aws_route53_record" "manager" {

  count                  = var.quantity-manager
  zone_id                = aws_route53_zone.main.zone_id
  name                   = "${var.managername}${count.index}.${aws_route53_zone.main.name}"
  type                   = "A"
  ttl                    = "300"
  records                = [element(aws_instance.manager.*.private_ip, count.index)]

}

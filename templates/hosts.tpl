[rke2managers]
%{ for index, ip in manager-ip ~}
rke2manager-${index} ansible_ssh_host=${ip}
%{ endfor }

[rke2workers]
%{ for index, ip in worker-ip ~}
rke2worker-${index} ansible_ssh_host=${ip}
%{ endfor }

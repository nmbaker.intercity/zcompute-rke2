# zCompute - RKE2 with High Availability

This script deploys RKE2 within the Zadara zCompute environment utilising Terraform and the Official AWS Module.

The instances are subsequently put into their roles Ansible Playbooks included within this repository.


## Prepare the Script

Update variables.tf with values specific to your environment and ensure you have network connectivity to the APIs listed within connect.tf

Ansible => 2.9.10 is required on the executing machine.

An SSH Key pair will need to be generated on your localhost with the Public uploaded to Compute > Key pairs within zCompute. You can manually specify a key path within ansible.cfg if required.


## Run the Script

This Example used the official Hashicorp AWS Provider https://registry.terraform.io/providers/hashicorp/aws/latest/docs

After you have adjust the variable.tf file you must run the following command
- terraform init
- terraform plan
- terraform apply


## File Descriptions
- connect.tf define the general connection to the defined zCloud environment.
- network.tf create the used security security group
- manager.tf create the master server
- worker.tf create the worker servers
- postscript.tf runs the asible playbook
- ansible.cfg define the ansible variables
- rke2_server.yaml is the Ansible script to install the prereqs and RKE2
- rke2_worker.yaml is the Ansible script to install the prereqs and join to the master server
- buffer/rke2manager is a buffer to store the token for joining the worker nodes
- inventory is the Ansible inventory
- templates define how the ips are stored in the inventory

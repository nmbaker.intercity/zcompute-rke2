
# My Account > Access Keys
variable "accesskey" {
  default = "123456123456123456123456"
}

# My Account > Access Keys
variable "secretkey" {
  default = "789789789789789789789789"
}

# DNS or IP for zCompute instance | No prefix or suffix
variable "zcloud_ip" {
  default = "my.zcompute.com"
}

# Number of RKE2 Control Plane Instances
variable "quantity-manager" {
  default = 3     # 1, 3 or 5 is recommended for etcd performance
}

# Number of RKE2 Worker Instances
variable "quantity-worker" {
  default = 3
}

# Instance name prefix for RKE2 Control Planes
variable "managername" {
  default = "rke2-master-"
}

# Instance name prefix for RKE2 Workers
variable "workername" {
  default = "rke2-worker-"
}

# Compute > Key Pairs (Private Key must be local to executing machine)
variable "keyname" {
  default = "My-Public-Key-for-zCompute"
}

# Compute > Instance Types
variable "instance" {
  default = "z4.xlarge"
}

# Machine Images > Images
variable "ami" {
    default = "ami-12345"
}

# Networking > VPCs
variable "vpc" {
  default = "vpc-12345"
}

# Networking > Subnets
variable "subnet" {
  default = "subnet-12345"
}

# zCompute Internal DNS Zone name 
variable "domainname" {
  default = "rke2.internal"
}
